from django.shortcuts import render, redirect
from .forms import Add_kabar
from .models import Kabar

# Create your views here.

def kabar_view(request, *args, **kwargs):
	my_form = Add_kabar()
	my_kabar = Kabar.objects.all()

	if request.method == 'POST':
		my_form = Add_kabar(request.POST)
		if my_form.is_valid():
			Kabar.objects.create(**my_form.cleaned_data)
			return redirect('kabar')
	context = {
		'form' : my_form,
		'kabar' : my_kabar
	}
	return render(request, 'kabar.html', context)