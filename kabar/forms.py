from django import forms

class Add_kabar(forms.Form):

	attr = {
	'class' : 'form-control',
	'placeholder' : 'Bagaimana Kabarmu?'
	}

	kabar = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=attr))