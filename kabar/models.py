from django.db import models
import datetime

# Create your models here.
class Kabar(models.Model):
	date = models.DateTimeField(auto_now=True)
	kabar = models.TextField(max_length=300)
