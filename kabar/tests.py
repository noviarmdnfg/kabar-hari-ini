from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve

from .views import kabar_view
from .models import Kabar
from .forms import Add_kabar

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.

class kabar_unit_test(TestCase):
	def test_kabar_url(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_redirect(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_func(self):
		found = resolve('/')
		self.assertEqual(found.func, kabar_view)

	def test_is_save(self):
		is_save = Kabar(id =1, date="2019-10-30 11:38", kabar="baik banget gila keren parah")
		is_save.save()

		self.assertIsInstance(is_save, Kabar)
		self.assertEqual(Kabar.objects.count(),1)

	def test_if_form_is_valid(self):
		form = Add_kabar(data={'kabar' : 'baikkkk bangettt'})
		self.assertTrue(form.is_valid)
		request = Client().post('/', data={'kabar' : 'baikkkk bangettt'})
		self.assertEqual(request.status_code, 302)

class kabar_functional_test(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')

		self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(kabar_functional_test, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(kabar_functional_test,self).tearDown()

	def test_kabar(self):
		driver = self.selenium

		driver.get('http://127.0.0.1:8000/')

		self.assertIn('Halo, Apa Kabar?', driver.page_source)
		
		kabar = driver.find_element_by_name('kabar')
		button = driver.find_element_by_tag_name('button')

		kabar.send_keys('gabut aing')
		button.click()

		self.assertIn('gabut aing', driver.page_source)