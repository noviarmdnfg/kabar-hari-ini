from django.urls import path
from . import views

app_name = 'booklist'
urlpatterns = [
    path('', views.booklist_view, name='booklist'),
    path('data/', views.data, name='books_data'),
]
