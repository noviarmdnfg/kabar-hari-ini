from django.shortcuts import render
from django.http import JsonResponse
import requests
import json


# Create your views here.
def data(request):
	try:
		q = request.GET['q']

	except:
		q = 'quilting'

	json_read = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + q).json()
	return JsonResponse(json_read)


def booklist_view(request):
    return render(request, 'booklist.html')