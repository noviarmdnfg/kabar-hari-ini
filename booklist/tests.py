from django.test import TestCase
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve

from .views import booklist_view

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
# Create your tests here.


class booklist_unit_test(TestCase):
    def test_status_url(self):
        response = Client().get('/booklist/')
        self.assertEqual(response.status_code, 200)

    def test_redirect(self):
        response = Client().get('/booklist/')
        self.assertEqual(response.status_code, 200)

    def test_func(self):
        found = resolve('/booklist/')
        self.assertEqual(found.func, booklist_view)

    def test_url_json_data(self):
        response = Client().get('/booklist/data/')
        self.assertEqual(response.status_code, 200)


class booklist_functional_test(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome(
            './chromedriver', chrome_options=chrome_options)
        super(booklist_functional_test, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(booklist_functional_test, self).tearDown()

    def test_halo(self):
        driver = self.selenium

        driver.get('http://127.0.0.1:8000/booklist/')

        self.assertIn('Daftar Buku', driver.page_source)