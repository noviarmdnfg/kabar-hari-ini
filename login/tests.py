from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import login_view, logout_view
from .forms import form_login
from django.contrib.auth.models import User
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


class login_unit_test(TestCase):
    def test_login_url(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_logout_url(self):
        response = Client().get('/login/logout')
        self.assertEqual(response.status_code,301)

    def test_func(self):
        found = resolve('/login/logout/')
        self.assertEqual(found.func, logout_view)
    
    def test_user_login(self):
        user = User.objects.create_user({'username' : 'testuser', 'password' : 'secret' })
        user.save()
        response = self.client.post('/login/', data={'username' : 'testuser', 'password' : 'secret'})
        self.assertEqual(response.status_code, 302)    
 
# class login_functional_test(LiveServerTestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')

#         self.selenium = webdriver.Chrome(
#             './chromedriver', chrome_options=chrome_options)
#         super(login_functional_test, self).setUp()

#     def tearDown(self):
#         self.selenium.quit()
#         super(login_functional_test, self).tearDown()

#     def test_login(self):
#         browser = self.selenium
#         browser.find_element_by_xpath('//*[@id="id_username"]').send_keys("kakpewe")
#         browser.find_element_by_xpath('//*[@id="id_password"]').send_keys("kfc261100")
#         browser.find_element_by_xpath('//*[@id="submit"]').click()
#         time.sleep(5)


#         self.assertIn("Selamat Datang,",driver.page_source)


