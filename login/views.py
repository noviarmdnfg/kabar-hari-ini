from django.shortcuts import render, redirect
from .forms import form_login
from django.contrib.auth import authenticate, login, logout

# Create your views here.
def logout_view(request):
    logout(request)
    form= form_login()
    response = {'form':form}
    return redirect('login')

def login_view(request):
    form = form_login()
    flag = False
    if request.method == "POST":
        form = form_login(request.POST)
        if form.is_valid():
            uname = form.cleaned_data['username']
            passw = form.cleaned_data['password']
            user = authenticate(request, username=uname, password = passw)
            if user is not None:
                login(request, user)
                flag = True
                response = {'form':form,'user':user,'flag':flag}
                return redirect('/login')
            else:
                response = {'form':form,'user':user,'flag':flag}
                return redirect('/login')
    else:
        response = {'form':form,'flag':flag}
        return render(request,'login.html',response)