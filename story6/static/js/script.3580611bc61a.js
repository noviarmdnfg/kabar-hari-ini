$(document).ready(function() {
    $('.content').hide();

    $('.toggle').click(function(e) {
        e.preventDefault();
    
      var $this = $(this);
    
      if ($this.next().hasClass('show')) {
          $this.next().removeClass('show');
          $this.next().slideUp(350);
      } else {
          $this.parent().parent().find('content').removeClass('show');
          $this.parent().parent().find('content').slideUp(350);
          $this.next().toggleClass('show');
          $this.next().slideToggle(350);
      }
    });
});