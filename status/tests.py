# Create your tests here.
from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve

from .views import status_view

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.

class kabar_unit_test(TestCase):
	def test_status_url(self):
		response = Client().get('/aktivitas/')
		self.assertEqual(response.status_code, 200)

	def test_redirect(self):
		response = Client().get('/aktivitas/')
		self.assertEqual(response.status_code, 200)

	def test_func(self):
		found = resolve('/aktivitas/')
		self.assertEqual(found.func, status_view)


class aktivitas_functional_test(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')

		self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(aktivitas_functional_test, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(aktivitas_functional_test,self).tearDown()

	def test_halo(self):
		driver = self.selenium

		driver.get('http://127.0.0.1:8000/aktivitas/')

		self.assertIn('Halo, Aku Novia', driver.page_source)

	def test_change_theme(self):
		driver = self.selenium
		driver.get('http://127.0.0.1:8000/aktivitas/')

		body_background = driver.find_element_by_tag_name('body').value_of_css_property('background-color')
		self.assertIn('rgba(182, 230, 189, 1)', body_background)

		driver.find_element_by_tag_name('button').click()
		body_background = driver.find_element_by_tag_name('body').value_of_css_property('background-color')
		self.assertIn('rgba(215, 209, 201, 1)', body_background)

	def test_accordion_1(self):
		driver = self.selenium
		driver.get('http://127.0.0.1:8000/aktivitas/')

		driver.find_element_by_id('aktivitas').click()
		self.assertIn('Ngerjain Tugas Kuliah',driver.page_source)

	def test_accordion_2(self):
		driver = self.selenium
		driver.get('http://127.0.0.1:8000/aktivitas/')

		driver.find_element_by_id('organisasi').click()
		self.assertIn('Controller DPM Fasilkom UI',driver.page_source)

	def test_accordion_3(self):
		driver = self.selenium
		driver.get('http://127.0.0.1:8000/aktivitas/')

		driver.find_element_by_id('prestasi').click()
		self.assertIn('Juara III Siswa berprestasi tingkat SMA IPS putri',driver.page_source)