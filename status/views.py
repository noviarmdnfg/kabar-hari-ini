from django.shortcuts import render

# Create your views here.

def status_view(request):
	return render(request, 'status.html')